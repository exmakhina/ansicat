#!/usr/bin/env python#
# SPDX-FileCopyrightText: 2012,2024 Jérôme Carretero <cJ-ansicat@zougloub.eu> & contributors
# SPDX-Licence-Identifier: BSD-2

import typing as t
import collections
import logging


logger = logging.getLogger(__name__)


def to_int(x, y=0):
	try: return int(x, 10)
	except: return y


def color_to_rgb(c):
	if c == 0:
		r, g, b = 128, 128, 128
	else:
		b = 0xff * ((c&4) >> 2)
		g = 0xff * ((c&2) >> 1)
		r = 0xff * ((c&1) << 0)
	return r, g, b

lut_vga = {
 30: (  0,  0,  0),
 31: (170,  0,  0),
 32: (  0,170,  0),
 33: (170, 85,  0),
 34: (  0,  0,170),
 35: (170,  0,170),
 36: (  0,170,170),
 37: (170,170,170),
 90: ( 85, 85, 85),
 91: (255, 85, 85),
 92: ( 85,255, 85),
 93: (255,255, 85),
 94: ( 85, 85,255),
 95: (255, 85,255),
 96: ( 85,255,255),
 97: (255,255,255),
}

lut_gray = [
 0x08, 0x12, 0x1c, 0x26, 0x30, 0x3a, 0x44, 0x4e, 0x58, 0x62, 0x6c, 0x76,
 0x80, 0x8a, 0x94, 0x9e, 0xa8, 0xb2, 0xbc, 0xc6, 0xd0, 0xda, 0xe4, 0xee,
]

def extcolor_to_rgb(cols):
	if not len(cols):
		logger.warning("Invalid SGR, need more: %s", cols[i:])
		return

	if cols[0] == "2":
		if len(cols) == 4: #RGB
			return (int(_) for _ in cols[1:1+3])
		else:
			logger.warning("Unimplemented SGR: %s", cols)
			return
	elif cols[0] == "5":
		if len(cols) == 2: #palette
			v = int(cols[1])
			if v in range(0,8):
				return lut_vga[30+v]
			elif v in range(8,16):
				return lut_vga[90+v-8]
			elif v in range(16,232):
				rg, b = divmod(v-16, 6)
				r, g = divmod(rg, 6)
				return r, g, b
			else:
				return [lut_gray[v-232]] * 3
		else:
			logger.warning("Unimplemented 8-bit SGR: %s", cols)
			return
	else:
		logger.warning("Unimplemented SGR: %s", cols)
		return

class Filter:
	def __init__(self):
		self.style_dict = {}
		self.x = 0
		self.y = 0
		self.pending: t.Sequence[int] = collections.deque()
		self.cursor_history = []
		self.color_history = []
		self.it = self.doit()

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		return

	def clear_line(self, param):
		logger.debug("Clear line %s", param)

	def clear_screen(self, param):
		logger.debug("Clear screen %s", param)

	def push_cursor(self, param):
		self.cursor_history.append((self.x, self.y))

	def pop_cursor(self, param):
		try:
			self.x, self.y = self.cursor_history.pop()
		except:
			raise

	def set_column(self, param):
		self.x = to_int(param, 1) - 1

	def carriage_return(self):
		self.x = 0

	def set_cursor(self, param):
		y, sep, x = param.partition(';')
		self.x = to_int(x, 1) - 1
		self.y = to_int(y, 1) - 1

	def move_cursor(self, x_offset=0, y_offset=0):
		logger.debug("Moving cursor by %d,%d", x_offset, y_offset)
		self.x += x_offset
		self.y += y_offset

	def move_up(self, param):
		self.move_cursor(y_offset = -to_int(param, 1))

	def move_down(self, param):
		self.move_cursor(y_offset = to_int(param, 1))

	def line_feed(self):
		self.add_char("\n")

	def move_left(self, param):
		self.move_cursor(x_offset = -to_int(param, 1))

	def backspace(self):
		self.move_left("")

	def tab(self):
		self.x = min(self.x // 8 * 8 + 8, self.width-1)

	def move_right(self, param):
		self.move_cursor(x_offset = to_int(param, 1))

	def next_line(self, param):
		self.move_cursor(y_offset = to_int(param, 1))

	def prev_line(self, param):
		self.move_cursor(y_offset = -to_int(param, 1))

	def push_color(self):
		self.color_history.append(self.style_dict.copy())

	def pop_color(self):
		self.style_dict = self.color_history.pop()

	def set_color(self, param):
		logger.debug("Set color: %s", param)
		cols = param.split(';')
		style = dict()
		i = 0
		for c in cols:
			c = to_int(cols[i], 0)
			if c in range(30,38):
				style['color'] = lut_vga[c]
			if c in range(90,98):
				style['color'] = lut_vga[c]
			elif c == 38:
				if (c := extcolor_to_rgb(cols[i+1:])) is not None:
					style['color'] = c
				else:
					return
			elif c in range(40,48):
				style['background'] = lut_vga[c-10]
			elif c in range(100,108):
				style['background'] = lut_vga[c-10]
			elif c == 48:
				if (c := extcolor_to_rgb(cols[i+1:])) is not None:
					style['background'] = c
				else:
					return
			elif c == 0:
				self.style_dict = {}
			elif c == 1:
				style['font-weight'] = 'bold'
			elif c == 2:
				style['font-weight'] = 'light'
			elif c == 22:
				style['font-weight'] = 'normal'
			elif c == 4:
				style['text-decoration'] = 'blink'
			elif c == 7:
				style['color'] = self.style_dict['background']
				style['background'] = self.style_dict['color']
			i += 1
		self.style_dict = self.style_dict | style
		self.apply_style()

	def apply_style(self):
		pass

	def show_cursor(self,param):
		return

	def hide_cursor(self,param):
		return

	def add_char(self, b):
		pass


	ansi_command_table = {
	 ("A"): "move_up",
	 ("B"): "move_down",
	 ("C"): "move_right",
	 ("D"): "move_left",
	 ("E"): "next_line",
	 ("F"): "prev_line",
	 ("G"): "set_column",
	 ("H"): "set_cursor",
	 ("f"): "set_cursor",
	 ("J"): "clear_screen",
	 ("K"): "clear_line",
	 ("h"): "show_cursor",
	 ("l"): "hide_cursor",
	 ("m"): "set_color",
	 ("s"): "push_cursor",
	 ("u"): "pop_cursor",
	}

	def doit(self):
		while True:
			if not self.pending:
				yield False
			c = self.pending.popleft()
			if c == "\r":
				self.carriage_return()
			elif c == "\n":
				self.line_feed()
			elif c == "\t":
				self.tab()
			elif c == "\x08":
				self.backspace()
			elif c == "\x1B":
				while not self.pending:
					yield False
				c = self.pending.popleft()
				if c == "[":
					command = ""
					while True:
						while not self.pending:
							yield False

						c = self.pending.popleft()
						if (cmd := self.ansi_command_table.get(c)) is not None:
							getattr(self, cmd)(command)
							break
						else:
							command += c
				else:
					logger.warning("Unknown escape, ignoring")
					self.add_char(c)
			else:
				self.add_char(c)

	def write(self, text):
		for c in text:
			self.pending.append(c)
		next(self.it)


