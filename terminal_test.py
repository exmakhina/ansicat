#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-ansicat@zougloub.eu> & contributors
# SPDX-Licence-Identifier: BSD-2

import io
import logging

from .terminal import *


logger = logging.getLogger(__name__)

def test_lf():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("123\nabc\n456")

		assert fo.getvalue() == "123\nabc\n456"

def test_cr():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("123\rabc")

		assert fo.getvalue() == "abc"

def test_longline():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("1" * 100)

		assert fo.getvalue() == ("1" * 80) + ("1" * 20)

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("1" * 50)
			f.write("\n")
			f.write("1" * 30)

		assert fo.getvalue() == ("1" * 50) + "\n" + ("1" * 30)

def test_cursor_left():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("Clearing line\n\x1B[31;1mclear line to cursor\x1B[1Kcontinue\x1B[5D\x1B[33;1mP\n")

		assert fo.getvalue() == "Clearing line\n                    conPinue\n"
		assert f.style_dict["color"] == (170, 85, 0)

def test_cursor_down():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write("1234567890\x1B[B1234567890")

		assert fo.getvalue() == "1234567890\n          1234567890"

def test_colors():
	f = TerminalFilter()

	def emit(t, s):
		logger.info("Now writing: %s", t.replace("\n", "\\n"))
		logger.info("Style: %s", s)

	f.emit = emit

	with f:
		f.write("\x1B[31;1mH\x1B[0m\x1B[32mello")

def test_cursor():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write(
			 "1234567890\n"
			 "1234567890\n"
			 "1234567890"
			 "\x1B[2A\x1B[DA" # write A instead of 1st zero
			 "\x1B[B\x1B[DB" # write B instead of 2nd zero
			 "\x1B[BC" # go to end and write C
			)

		assert fo.getvalue() == "123456789A\n123456789B\n1234567890C"


def test_cursorstack():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write(
			 "1234567890\n"
			 "1234567890\n"
			 "1234567890"
			 "\x1B[s\x1B[1,1HA" # write A instead of 1st 1
			 "\x1B[uC" # go to end and write C
			)

		assert fo.getvalue() == "A234567890\n1234567890\n1234567890C"

def test_colorstack():
	pass

def test_tab():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write(
			 "a\tb"
			)

		assert fo.getvalue() == "a" + (" " * 7) + "b"

def test_tab_eol():
	f = TerminalFilter()

	with io.StringIO() as fo:
		def emit(t, s):
			logger.info("Now writing: %s", t.replace("\n", "\\n"))
			fo.write(t)

		f.emit = emit

		with f:
			f.write(
			 ("\t" * 50) +
			 "a"
			)

		assert fo.getvalue() == (" " * (f.width - 1)) + "a"
