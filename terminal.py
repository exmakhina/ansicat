#!/usr/bin/env python#
# SPDX-FileCopyrightText: 2012,2024 Jérôme Carretero <cJ-ansicat@zougloub.eu> & contributors
# SPDX-Licence-Identifier: BSD-2

import typing as t
import collections
import logging
import unicodedata

from .filter import (
 Filter,
 to_int,
)


logger = logging.getLogger(__name__)


class TerminalFilter(Filter):
	def __init__(self, width=80, height=25):
		super().__init__()
		self.width = width
		self.height = height

		self.reset()

	def reset(self):
		width, height = self.width, self.height
		self.lines = collections.deque()
		self.style = collections.deque()
		for y in range(height):
			rowt = []
			rows = []
			for x in range(width):
				rowt.append(None)
				rows.append(None)
			rowt.append(None)
			self.lines.append(rowt)
			self.style.append(rows)

		self.maxy = 0
		self.x = 0
		self.y = 0

	def __enter__(self):
		self.reset()
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		self._eof()

	def _eof(self):
		for i in range(self.maxy+1):
			self.emit_line()
			self.maxy -= 1
			self.y -= 1

	def emit_line(self):
		text = self.lines.popleft()
		style = self.style.popleft()
		self.lines.append([None] * (self.width+1))
		self.style.append([None] * self.width)

		maxnone = self.width
		for i in range(self.width-1, -1, -1):
			if text[i] is not None:
				break
			maxnone = i

		data = []
		for i, c in zip(range(maxnone), text):
			if c is None:
				c = " "
			data.append(c)

		l = "".join(data)
		if text[-1] or (maxnone < self.width and self.maxy > 0):
			l += "\n"

		self.emit(l, style)

	def emit(self, line, style=None):
		logger.debug("Emit: %s", line.encode())

	def line_feed(self):
		logger.debug("Line feed")
		self.lines[self.y][self.width] = True
		if self.y == self.height-1:
			logger.debug("on last line")
			self.emit_line()
		else:
			self.y += 1
		self.x = 0
		self.maxy = max(self.y, self.maxy)

	def set_cursor(self, param):
		y, sep, x = param.partition(';')
		self.x = to_int(x, 1) - 1
		self.y = to_int(y, 1) - 1
		logger.debug("Moving cursor to %d,%d", self.x, self.y)
		self.maxy = max(self.maxy, self.y)

	def move_cursor(self, x_offset=0, y_offset=0):
		logger.debug("Moving cursor by %d,%d", x_offset, y_offset)
		self.x += x_offset
		if y_offset > 0:
			for i in range(y_offset):
				self.lines[self.y][-1] = True
				self.y += 1
		else:
			self.y += y_offset
		self.maxy = max(self.maxy, self.y)

	def clear_line(self, param):
		mode = to_int(param, 0)
		logger.debug("Clearing line (%d)", mode)
		line = self.lines[self.y]
		if mode == 1: # Clear from begining of line to cursor position
			line[:self.x] = [None] * self.x
		elif mode == 2: # Clear entire line
			line[:-1] = [None] * self.width
		else: # Clear from cursor position to end of line
			line[self.x:-1] = [None] * (self.width-self.x)

	def clear_screen(self, param):
		mode = to_int(param, 0)
		logger.debug("Clearing screen (%d)", mode)
		# TODO impact on lf
		raise NotImplementedError(mode)
		if mode == 1: # Clear from begining of screen to cursor position
			pass
		elif mode == 2: # Clear entire screen and return cursor to home
			pass
		else: # Clear from cursor position to end of screen
			pass
		raise Exception('Todo')

	def add_char(self, b):
		logger.debug("@ %d,%d/%d add char %s", self.x, self.y, self.maxy, b)
		w = unicodedata.east_asian_width(b)

		if w in ("W", "F"):
			dx = 2
			entry = [b,'']
		else:
			entry = [b]
			dx = 1

		if self.x + dx > self.width:
			if self.y == self.height-1:
				self.emit_line()
			else:
				self.y += 1
				self.maxy = max(self.maxy, self.y)
			self.x = 0
		line = self.lines[self.y]
		style = self.style[self.y]
		for c in entry:
			line[self.x] = c
			style[self.x] = self.style_dict
			self.x += 1

