#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incomedeclaration@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging
import sys
import os
import io


logger = logging.getLogger(__name__)


def main(argv=None):
	import argparse
	parser = argparse.ArgumentParser(
	 description="ANSI Escape Codes Processor",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "html",
	 help="Convert to html",
	)
	subp.add_argument(
	 "-t", "--title",
	 default='Terminal output',
	 help="HTML page title",
	)
	def doit(args):
		from .html import Ansi2Html
		f = Ansi2Html(sys.stdout, title=args.title)
		with f:
			while True:
				x = sys.stdin.readline()
				if not x:
					break
				f.write(x)

	subp.set_defaults(func=doit)


	args = parser.parse_args(argv)

	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		 logger=logging.root,
		 isatty=True,
		)
	except ImportError:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	func = getattr(args, 'func', None)
	if func is not None:
		args.func(args)
	else:
		parser.print_help()
		return 1


if __name__ == "__main__":
	raise SystemExit(main())
