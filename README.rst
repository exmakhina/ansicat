#######
Ansicat
#######


This tool / library is helpful to process
`ANSI escape codes <https://en.wikipedia.org/wiki/ANSI_escape_code>`_,
for example to transform colored terminal output into other formats,
or to strip colors.


Usage
#####

.. code:: sh

   printf "Hello \x1B[31;1mdangerous\x1B[0m world\n" | python -m exmakhina.ansicat html


Architecture
############

- Based on a filter implementing callbacks.

- Fundamentally this library is working with unicode strings. You may
  use encoding and decoding using surrogate escapes if bytes
  processing is really needed.

- Incremental writing is supported.

- Cursor positioning considers `full-width characters
  <https://en.wikipedia.org/wiki/Halfwidth_and_Fullwidth_Forms_(Unicode_block)>`_
  which take two characters.


Credits
#######

The initial code was based on the `ansiterm.py` file, an ANSI terminal
interpreter for Win32 console, which is found in the `waf <http://waf.io/>`_
build system.

