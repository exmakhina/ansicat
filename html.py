#!/usr/bin/env python#
# SPDX-FileCopyrightText: 2012,2024 Jérôme Carretero <cJ-ansicat@zougloub.eu> & contributors
# SPDX-Licence-Identifier: BSD-2

import typing as t
import sys, os, re
import logging

from .filter import Filter
from .terminal import TerminalFilter


logger = logging.getLogger(__name__)


def rgb2html(r, g, b):
	return f"#{r:02x}{g:02x}{b:02x}"


class Ansi2Html(TerminalFilter):
	def __init__(self, buf, title=None, default_style: t.Mapping[str,str]=None):
		super().__init__()
		self.buffer = buf
		self.last_style = None
		self.style_dict = {}
		if default_style is None:
			default_style = {
			 'color': '#a0a0a0',
			 'background': '#000000',
			 'font-family': 'Courier New',
			 'font-size': '10pt',
			}
		self.default_style = default_style
		if title is None:
			title = "Console output"
		self.title = title

	def __enter__(self):
		self._write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">\n')
		self._write("<html>\n")
		self._write("<head>\n")
		self._write(' <meta http-equiv="content-type" content="text/html; charset=utf-8">\n')
		self._write(' <title>')
		self._write(self.title)
		self._write('</title>\n')
		self._write("</head>\n")
		self._write('<body style="')
		for k, v in self.default_style.items():
			self._write(("%s:%s;" % (k, v)))
		self._write('"><pre>\n')
		super().__enter__()
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		super().__exit__(exc_type, exc_value, exc_tb)
		if self.last_style is not None:
			self._write('</span>')
		self._write('</pre></body></html>\n')

	def add_char___(self, x):
		self._write(x)

	def apply_style___(self):
		return
		if self.style_dict == self.last_style:
			return

		if self.last_style is None:
			pass
		else:
			self._write('</span>')

		if self.style_dict == {}:
			attrs = None
		else:
			attrs = []
			for k, v in self.style_dict.items():
				attrs.append("%s:%s;" % (k, v))
			attrs = "".join(attrs)
			self._write(f"<span style=\"{attrs}\">")
		self.last_style = attrs

	def emit(self, txt, sty):
		logger.debug("Emit %s %s", txt, sty)
		for t, s in zip(txt, sty):
			if s == self.last_style:
				pass
			else:
				if self.last_style is None:
					pass
				else:
					self._write('</span>')

				if not s:
					attrs = None
				else:
					attrs = []
					for k, v in s.items():
						if k in ("color", "background"):
							v = rgb2html(*v)
						attrs.append("%s:%s;" % (k, v))
					attrs = "".join(attrs)
					self._write(f"<span style=\"{attrs}\">")
			self.last_style = s
			self._write(t)

	def _write(self, txt):
		self.buffer.write(txt)
