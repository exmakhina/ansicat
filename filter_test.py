#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-ansicat@zougloub.eu> & contributors
# SPDX-Licence-Identifier: BSD-2

import logging

from .filter import *


logger = logging.getLogger(__name__)


def test_Filter():
	f = Filter()

	def write(x):
		logger.info("Now writing: %s", x)

	f.add_text = write
	with f:
		f.write("\x1B[31;1mHello\x1B[0m")


def test_Filter_incremental():
	f = Filter()

	def write(x):
		logger.info("Now writing: %s", x)

	f.add_text = write
	with f:
		f.write("\x1B[3")
		f.write("1;1mHello\x1B[0m")


